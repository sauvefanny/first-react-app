import './App.css';
import {Greeting } from'./components/Greeting';
import {Count} from './components/Counter';


function App() {
  return (
    <div>
      <h1>My First React App</h1>
        <Greeting name = 'Fanny'/>
        <Greeting name = 'Quentin'/>
        <Greeting name = 'Nassrine'/>
        <Count />

    </div>
  );
}

export default App;


