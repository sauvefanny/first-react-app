import {useState} from 'react';

export function Count(){
    const [count, setCount] = useState(0);
  
    return(
      <div>
        <p>vous avez cliqué {count} fois</p>
        <button onClick = {() => setCount(count + 1 )}>
          Incrément
        </button>
        <button onClick = {() => setCount(count - 1 )}>
          Décrément
        </button>
        <button onClick = {() => setCount(0)}>
          Reset
        </button>
      </div>
    );
  }